@extends('admin.layouts.app')
@section('title','Orderbikes')
@section('content')
    <div class="card" style="padding: 32px;">

        <h1>
            Đặt xe
        </h1>

        <div>
            <div class="row">
                <div>
                    <!-- Trigger the modal -->
                    <button type="button" class="btn btn-primary" style="background-color:dodgerblue" data-bs-toggle="modal"
                     data-bs-target="#addCustomerModal">
                     Thêm khách hàng mới
                    </button>
                </div>

                <div class="col-md-6">
                    <a href="{{ route('orderbikes.create') }}" class="btn btn-primary" style="background-color:green">Create</a>
                </div>

                <div class="col-md-6">
                    <form method="get" action="{{ route('orderbikes.search') }}">
                        <div class="input-group">
                            <input class="form-control" name="search" placeholder="Search ..."
                            style="
                            border: 1px solid #ccc;
                            padding: 10px;
                            border-top-right-radius: 0 !important;
                            border-bottom-right-radius: 0px !important;"
                            value="{{ $search ?? '' }}">
                            <button type="submit" class="btn btn-primary" style="margin-left: 0px;margin-bottom: 0px;">Search</button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="table-responsive">
                <table class= "table table-hover" style="vertical-align: middle; border: 1px solid #134ca1;">
                    <tr class="table-primary">
                        <th>STT</th>
                        <th>Tên khách hàng</th>
                        <th>Tên xe</th>
                        <th>Biển số</th>
                        <th>Giá thuê</th>
                        <th>Ngày bắt đầu thuê</th>
                        <th>Ngày dự kiến trả</th>
                        <th>Trạng thái</th>
                        <th>Hành động</th>
                    </tr>
                    @php
                        $index = 1;
                    @endphp
                    @foreach ($orderbikes as $orderbike)
                    <tr>
                        <td>{{ $index }}</td>
                        <td>{{ $orderbike->customer->hoten}}</td>
                        <td>{{ $orderbike->orderbike_bike->tenxe}}</td>
                        <td>{{ $orderbike->orderbike_bike->bienso }}</td>
                        <td>{{ $orderbike->orderbike_bike->giathue }}</td>
                        <td>{{ $orderbike->ngaybatdauthue }}</td>
                        <td>{{ $orderbike->ngaydutra }}</td>
                        <td>{{ $orderbike->status }}</td>
                        <td >
                            @if ($orderbike->status != 'Đã thanh toán') <!-- Kiểm tra trạng thái -->
                            <a href="{{ route('orderbikes.edit', $orderbike->id) }}" style="text-decoration: none;">
                                <i style="color: black;" class="fas fa-pen"></i>
                            </a>
                            <form action="{{ route('orderbikes.destroy', $orderbike->id) }}" id="form-delete{{ $orderbike->id }}"
                                method="post" style="margin: 0;" class="d-inline">
                                @csrf
                                @method('delete')
                                <button class="btn btn-link btn-delete" style="margin: 0;" data-id={{ $orderbike->id }}>
                                    <i class="fas fa-trash" style="color: red;"></i>
                                </button>
                            </form>
                            <a href="{{ route('orderbikes.show', $orderbike->id) }}" style="text-decoration: none;">
                                <i style="color:black;" class="fas fa-eye"></i>
                            </a>
                            @endif

                            @if ($orderbike->status == 'Đã giao xe') <!-- Kiểm tra trạng thái -->
                            <a href="{{ route('bills.create', ['orderbike' => $orderbike->id, 'tamung' => $orderbike->tamung, 'ngaybatdauthue' => $orderbike->ngaybatdauthue]) }}" >
                                {{-- class="btn btn-primary" style="margin: 0;">Pay</a> --}}
                                <i class="fas fa-credit-card"  style="margin-left: 10px;"></i></a>
                            @endif
                        </td>
                    </tr>
                    @php
                        $index++;
                    @endphp
                    @endforeach
               </table>
                {{ $orderbikes->links() }}
            </div>
        </div>
        @Include('admin.orderbike.modal')
    </div>
@endsection
@push('script')
 <!-- Include jQuery (if not already included) -->
 <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
 <!-- Include custom.js -->
 <script src="{{ asset('admin/assets/js/custom.js') }}"></script>
<script>
    @if (session('message'))
             // alert('{{session('message') }}');
             Swal.fire({
                 position: "top-end",
                 icon: '{{session('statuscode') }}',
                 title: '{{session('message') }}',
                 showConfirmButton: false,
                 timer: 1500,
             })

     @endif

 </script>
@endpush

