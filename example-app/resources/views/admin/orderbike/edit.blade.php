@extends('admin.layouts.app')
@section('title','Edit Orderbikes')
@section('content')
    <div class="card">
        <h1>Chỉnh sửa thông tin đặt xe</h1>
        <div>
            <form action="{{ route('orderbikes.update', $orderbikes->id) }}" method="POST">
                @csrf
                @method('PUT')

                <div class="input-group input-group-static mb-4">
                    <label name = "group" class="ms-0">Họ và tên khách hàng</label>
                    <select name="customer_id" class="form-control">
                        @foreach ($customers as $customer)
                        <option value={{ $customer->id }} {{ old('customer_id', $orderbikes->customer_id) == $customer->id ? 'selected' : '' }}>{{ $customer->hoten }}</option>
                        @endforeach

                        @error('customer_id')
                        <span class="text-danger"> {{ $message}}</span>
                        @enderror
                    </select>
                </div>

                <div class="input-group input-group-static mb-4">
                    <label name="group" class="ms-0">Chọn xe</label>
                    <select name="motorbike_id" class="form-control">
                        @if ($motorbikeOld->id == old('motorbike_id', $orderbikes->motorbike_id))
                            <option value={{ $motorbikeOld->id }}>{{ $motorbikeOld->tenxe }} - {{ $motorbikeOld->bienso }} - {{ $motorbikeOld->status }}</option>
                        @endif
                        @foreach ($motorbikes as $motorbike)
                            <option value="{{ $motorbike->id }}" {{$motorbike->id == $orderbikes->motorbike_id ? 'selected' : '' }}>
                                {{ $motorbike->tenxe }} - {{ $motorbike->bienso }} - {{ $motorbike->status }}
                            </option>
                        @endforeach
                    </select>
                </div>

                <div class="input-group input-group-static mb-4">
                    <label>Ngày bắt đầu thuê</label>
                    <input type="datetime-local" class="form-control" value="{{ old('ngaybatdauthue', $orderbikes->ngaybatdauthue) }}" 
                            name="ngaybatdauthue">

                    @error('ngaybatdauthue')
                        <span class="text-danger"> {{ $message}}</span>
                    @enderror

                </div>

                <div class="input-group input-group-static mb-4">
                    <label>Ngày dự trả</label>
                    <input type="datetime-local" class="form-control" value="{{ old('ngaydutra', $orderbikes->ngaydutra) }}" 
                            name="ngaydutra">

                    @error('ngaydutra')
                        <span class="text-danger"> {{ $message}}</span>
                    @enderror

                </div>

                <div class="input-group input-group-static mb-4">
                    <label>Tạm ứng</label>
                    <input type="text" class="form-control" value="{{ old('tamung',$orderbikes->tamung) }}" name="tamung" placeholder="0">

                    @error('tamung')
                        <span class="text-danger"> {{ $message}}</span>
                    @enderror

                </div>

                <div class="input-group input-group-static mb-4">
                    <label>Ghi chú</label>
                    <input type="text" class="form-control" value="{{ old('ghichu',$orderbikes->ghichu) }}" name="ghichu" placeholder="Ghi chú">

                    @error('ghichu')
                        <span class="text-danger"> {{ $message}}</span>
                    @enderror

                </div>
                <div class="input-group input-group-static mb-4">
                    <label>Trạng thái</label>
                    <select name="status" class="form-control">
                        <option value="Đã giao xe" {{ old('status', $orderbikes->status) === 'Đã giao xe' ? 'selected' : '' }}>Đã giao xe</option>
                        <option value="Chưa giao xe" {{ old('status', $orderbikes->status) === 'Chưa giao xe' ? 'selected' : '' }}>Chưa giao xe</option>
                    </select>
                
                    @error('status')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
                <button type="submit" class="btn btn-submit btn-primary">Cập nhật</button>
            </form>
        </div>
    </div>
    
@endsection