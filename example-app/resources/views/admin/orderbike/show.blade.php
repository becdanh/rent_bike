@extends('admin.layouts.app')
@section('title','Orderbikes details')
@section('content')
    <div class="card" style="padding: 32px;">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Chi tiết đặt xe</h4>
                        <a href="{{ url()->previous() }}" class="btn btn-primary">Back</a>
                        <a href="{{ route('orderbikes.edit', $orderbikes->id) }}" class="btn btn-primary">edit</a>
                        <hr>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <label for="">Tên khách hàng</label>
                                <div class="p-2 border">{{ $orderbikes->customer->hoten}}</div>
                            </div>
                            <div class="col-md-4">
                                <label for="">Căn cước công dân</label>
                                <div class="p-2 border">{{ $orderbikes->customer->cccd}}</div>
                            </div>
                            <div class="col-md-4">
                                <label for="">Bằng lái</label>
                                <div class="p-2 border">{{ $orderbikes->customer->banglai}}</div>
                            </div>
                            <div class="col-md-4">
                                <label for="">Tên xe</label>
                                <div class="p-2 border">{{ $orderbikes->orderbike_bike->tenxe}}</div>
                            </div>
                            <div class="col-md-4">
                                <label for="">Biển số</label>
                                <div class="p-2 border">{{ $orderbikes->orderbike_bike->bienso }}</div>
                            </div>
                            <div class="col-md-4">
                                <label for="">Giá thuê</label>
                                <div class="p-2 border">{{ $orderbikes->orderbike_bike->giathue }}</div>
                            </div>
                            <div class="col-md-4">
                                <label for="">Ngày bắt đầu thuê</label>
                                <div class="p-2 border">{{ $orderbikes->ngaybatdauthue }}</div>
                            </div>
                            <div class="col-md-4">
                                <label for="">Ngày dự kiến trả</label>
                                <div class="p-2 border">{{ $orderbikes->ngaydutra }}</div>
                            </div>
                            <div class="col-md-4">
                                <label for="">Ghi chú</label>
                                <div><textarea class="p-2 border" cols="34" readonly>{{ $orderbikes->ghichu }}</textarea></div>
                            </div>
                            <div class="col-md-4">
                                <label for="">Tạm ứng</label>
                                <div class="p-2 border">{{ $orderbikes->tamung }}</div>
                            </div>
                            <div class="col-md-4">
                                <label for="">Trạng thái</label>
                                <div class="p-2 border">{{ $orderbikes->status }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                
    </div>

@endsection
