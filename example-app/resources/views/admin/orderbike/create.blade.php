@extends('admin.layouts.app')
@section('title','Create Orderbikes')
@section('content')
    <div class="card" style="padding: 32px;">
        <h1>Cho thuê</h1>
        <div>
            <form action="{{ route('orderbikes.store')}}" method="POST">
                @csrf
                <div class="input-group input-group-static mb-4">
                    <label name = "group" class="ms-0">Họ và tên khách hàng</label>
                    <select name="customer_id" class="form-control">
                        @foreach ($customers as $customer)
                        <option value={{ $customer->id }}>{{ $customer->hoten }}</option>
                        @endforeach

                        @error('customer_id')
                        <span class="text-danger"> {{ $message}}</span>
                    @enderror
                    </select>

                </div>

                <div class="input-group input-group-static mb-4">
                    <label name = "group" class="ms-0">Chọn xe</label>
                    <select name="motorbike_id" class="form-control">
                        @foreach ($motorbikes as $motorbike)
                        <option value="{{ $motorbike->id}}">{{ $motorbike->tenxe }} - {{ $motorbike->bienso }} - {{ $motorbike->status }}</option>
                        
                        @endforeach
                    </select>
                </div>



                <div class="input-group input-group-static mb-4">
                    <label>Ngày bắt đầu thuê</label>
                    <input type="datetime-local" class="form-control" value="{{ old('ngaybatdauthue') }}" name="ngaybatdauthue" placeholder="Ngày bắt đầu thuê">

                    @error('ngaybatdauthue')
                        <span class="text-danger"> {{ $message}}</span>
                    @enderror

                </div>

                <div class="input-group input-group-static mb-4">
                    <label>Ngày dự kiến trả</label>
                    <input type="datetime-local" class="form-control" value="{{ old('ngaydutra') }}" name="ngaydutra" placeholder="Ngày dự kiến trả">

                    @error('ngaydutra')
                        <span class="text-danger"> {{ $message}}</span>
                    @enderror

                </div>

                <div class="input-group input-group-static mb-4">
                    <label>Tạm ứng</label>
                    <input type="text" class="form-control" value="{{ old('tamung','0') }}" name="tamung" placeholder="">

                    @error('tamung')
                        <span class="text-danger"> {{ $message}}</span>
                    @enderror

                </div>

                <div class="input-group input-group-static mb-4">
                    <label>Ghi chú</label>
                    <input type="text" class="form-control" value="{{ old('ghichu') }}" name="ghichu" placeholder="Ghi chú">

                    @error('ghichu')
                        <span class="text-danger"> {{ $message}}</span>
                    @enderror

                </div>

                <button type="submit" name="action" value="khonggiao" class="btn btn-submit btn-primary">Thêm mới</button>
                <button type="submit" name="action" value="thuexe" class="btn btn-submit btn-primary">Thêm mới và giao xe</button>
            </form>
        </div>
    </div>
    
@endsection