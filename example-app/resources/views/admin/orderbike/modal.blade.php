<div class="modal fade" id="addCustomerModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Thêm khách hàng mới </h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
            <form id="addCustomerForm" action="{{ route('customers.storeAjax') }}" method="POST" enctype="multipart/form-data" >
                @csrf
                <div class="modal-body">
                    <div class="input-group input-group-static mb-4">
                        <label>Họ và tên</label>
                        <input type="text" class="form-control" value="{{ old('hoten') }}" name="hoten" placeholder="Họ và tên">
                        <span class="text-danger"></span>
    
                        @error('hoten')
                            <span class="text-danger"> {{ $message}}</span>
                        @enderror
    
                    </div>
    
                    <div class="input-group input-group-static mb-4">
                        <label>Số điện thoại</label>
                        <input type="text" class="form-control" value="{{ old('sdt') }}" name="sdt" placeholder="Số điện thoại">
                        <span class="text-danger"></span>
    
                        @error('sdt')
                            <span class="text-danger"> {{ $message}}</span>
                        @enderror
    
                    </div>
                    
                    <div class="input-group input-group-static mb-4">
                        <label>Căn cước công dân</label>
                        <input type="text" class="form-control" value="{{ old('cccd') }}" name="cccd" placeholder="Căn cước công dân">
                        <span class="text-danger"></span>
    
                        @error('cccd')
                            <span class="text-danger"> {{ $message}}</span>
                        @enderror
    
                    </div>
    
                    <div class="input-group input-group-static mb-4">
                        <label>Bằng lái</label>
                        <input type="text" class="form-control" value="{{ old('banglai') }}" name="banglai" placeholder="Bằng lái">
                        <span class="text-danger"></span>
    
                        @error('banglai')
                            <span class="text-danger"> {{ $message}}</span>
                        @enderror
    
                    </div>
                    
                    <div class="row">
                        <div class="input-group-static col-5 mb-4">
                            <label>Ảnh mặt trước</label>
                            <input type="file" class="form-control" value="{{ old('cccd1') }}" name="cccd1" id="image-input" placeholder="thêm ảnh">
                            <span class="text-danger"></span>
        
                            @error('cccd1')
                                <span class="text-danger"> {{ $message }}</span>
                            @enderror
                        </div>
                        <div class="col-5">
                            <img src="{{ asset('../imagecustomers/no-image.png')}}" id="show-image" alt="" width="200" height="140">
                        </div>
                  </div>
    
    
                    <div class="row">
                        <div class="input-group-static col-5 mb-4">
                            <label>Ảnh mặt sau</label>
                            <input type="file" class="form-control" value="{{ old('cccd2') }}" name="cccd2" id="image-input1" placeholder="thêm ảnh">
                            <span class="text-danger"></span>
        
                            @error('cccd2')
                                <span class="text-danger"> {{ $message }}</span>
                            @enderror
                        </div>
                        <div class="col-5">
                            <img src="{{ asset('../imagecustomers/no-image.png')}}" id="show-image1" alt="" width="200" height="140">
                        </div>
                    </div>
    
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn btn-primary" id="addCustomerBtn">Thêm mới</button>

                </div>
            </form>
      </div>
    </div>
  </div>
