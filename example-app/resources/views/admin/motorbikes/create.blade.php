@extends('admin.layouts.app')
@section('title','Create motorbikes')
@section('content')
    <div class="card">
        <h1>Thêm xe mới</h1>
        <div>
            <form action="{{ route('motorbikes.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="input-group input-group-static mb-4">
                    <label>Tên xe</label>
                    <input type="text" class="form-control" value="{{ old('tenxe') }}" name="tenxe" placeholder="Tên xe">

                    @error('tenxe')
                        <span class="text-danger"> {{ $message}}</span>
                    @enderror

                </div>

                <div class="input-group input-group-static mb-4">
                    <label>Hãng xe</label>
                    <input type="text" class="form-control" value="{{ old('hangxe') }}" name="hangxe" placeholder="Hãng xe">

                    @error('hangxe')
                        <span class="text-danger"> {{ $message }}</span>
                    @enderror

                </div>

                <div class="input-group input-group-static mb-4">
                    <label>Biển số</label>
                    <input type="text" class="form-control" value="{{ old('bienso') }}" name="bienso" placeholder="Biển số">

                    @error('bienso')
                        <span class="text-danger"> {{ $message }}</span>
                    @enderror

                </div>

                <div class="input-group input-group-static mb-4">
                    <label>Giá thuê</label>
                    <input type="text" class="form-control" value="{{ old('giathue') }}" name="giathue" placeholder="Giá thuê">

                    @error('giathue')
                        <span class="text-danger"> {{ $message }}</span>
                    @enderror

                </div>

                {{-- <div class="input-group input-group-static mb-4">
                    <label>Trạng thái</label>
                    <input type="text" class="form-control" value="{{ old('status') }}" name="status" placeholder="Trạng thái">

                    @error('status')
                        <span class="text-danger"> {{ $message }}</span>
                    @enderror

                </div> --}}

                <div class="input-group input-group-static mb-4">
                    <label name="group" class="ms-0">Loại xe</label>
                    <select name="classifybike_id" class="form-control">
                        @foreach ($classifys as $item)
                            <option value="{{ $item->id }}">{{ $item->tenphanloai }}</option>
                        @endforeach
                    </select>

                    @error('classifybike_id')
                        <span class="text-danger"> {{ $message }}</span>
                    @enderror
                </div>

              <div class="row">
                <div class="input-group-static col-5 mb-4">
                    <label>Hình ảnh</label>
                    <input type="file" class="form-control" value="{{ old('file_upload') }}" name="file_upload" id="image-input" placeholder="thêm ảnh">

                    @error('file_upload')
                        <span class="text-danger"> {{ $message }}</span>
                    @enderror

                </div>
                <div class="col-5">
                    <img src="{{ asset('../productImg/no-image.png')}}" id="show-image" alt="" width="200" height="140">
                </div>
              </div>


                <button type="submit" class="btn btn-submit btn-primary">Thêm mới</button>
            </form>
        </div>
    </div>

@endsection


