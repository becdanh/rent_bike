@extends('admin.layouts.app')
@section('title','Edit motorbikes' . $motorbikes->tenxe)
@section('content')
    <div class="card">
        <h1>Chỉnh sửa thông tin xe</h1>
        <div>
            <form action="{{ route('motorbikes.update', $motorbikes->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="input-group input-group-static mb-4">
                    <label>Tên xe</label>
                    <input type="text" class="form-control" value="{{ old('tenxe',$motorbikes->tenxe) }}" name="tenxe" placeholder="Tên xe">

                    @error('tenxe')
                        <span class="text-danger"> {{ $message}}</span>
                    @enderror

                </div>

                <div class="input-group input-group-static mb-4">
                    <label>Hãng xe</label>
                    <input type="text" class="form-control" value="{{ old('hangxe', $motorbikes->hangxe) }}" name="hangxe" placeholder="Hãng xe">

                    @error('hangxe')
                        <span class="text-danger"> {{ $message}}</span>
                    @enderror

                </div>

                <div class="input-group input-group-static mb-4">
                    <label>Biển số</label>
                    <input type="text" class="form-control" value="{{ old('bienso', $motorbikes->bienso) }}" name="bienso" placeholder="Biển số">

                    @error('bienso')
                        <span class="text-danger"> {{ $message}}</span>
                    @enderror

                </div>

                <div class="input-group input-group-static mb-4">
                    <label>Giá</label>
                    <input type="text" class="form-control" value="{{ old('giathue',$motorbikes->giathue) }}" name="giathue" placeholder="Gía">

                    @error('giathue')
                        <span class="text-danger"> {{ $message}}</span>
                    @enderror

                </div>

                <div class="input-group input-group-static mb-4">
                    <label>Trạng thái</label>
                    <select name="status" class="form-control">
                        <option value="Còn trống" {{ old('status', $motorbikes->status) === 'Còn trống' ? 'selected' : '' }}>Còn trống</option>
                        <option value="Đang bận" {{ old('status', $motorbikes->status) === 'Đang bận' ? 'selected' : '' }}>Đang bận</option>
                    </select>

                    @error('status')
                        <span class="text-danger"> {{ $message}}</span>
                    @enderror

                </div>


                <div class="input-group input-group-static mb-4">
                    <label name="group" class="ms-0">Loại xe</label>
                    <select name="classifybike_id" class="form-control">
                        {{-- <option value="">{{$motorbikes->classifybike_id}}</option> --}}
                        @foreach ($classifys as $item)
                            <option value="{{ $item->id }}" {{ $motorbikes->classifybike_id == $item->id ? 'selected' : '' }}>{{ $item->tenphanloai }}</option>
                        @endforeach
                    </select>

                    @error('classifybike_id')
                        <span class="text-danger"> {{ $message }}</span>
                    @enderror
                </div>

                <div class="row">
                    <div class="input-group-static col-5 mb-4">
                        <label>Hình ảnh</label>
                        <input type="file" class="form-control" name="file_upload" value="{{ old('file_upload') }}"  id="image-input" placeholder="thêm ảnh">

                        @error('file_upload')
                            <span class="text-danger"> {{ $message }}</span>
                        @enderror
                    </div>
                    <div class="col-5">
                        @if ($motorbikes->image)
                        {{-- <img src="{{ asset('../productImg/'.$motorbikes->image) }}" alt="Current Image" width="200"> --}}
                        <img src="{{ asset('../productImg/'.$motorbikes->image)}}" id="show-image" alt="" width="200" height="140">
                        @endif
                    </div>
                  </div>


                <button type="submit" class="btn btn-submit btn-primary">Cập nhật</button>
            </form>
        </div>
    </div>

@endsection



