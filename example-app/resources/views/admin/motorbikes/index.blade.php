@extends('admin.layouts.app')
@section('title', 'motorbikes')
@section('content')
    <div class="card" style="padding: 32px;">

        <h1>
            Danh sách xe
        </h1>

        <div>
            <div class="row">
                <div class="col-md-6">
                    <a href="{{ route('motorbikes.create') }}" class="btn btn-primary"
                        style="background-color:green">Create</a>
                </div>
                <div class="col-md-6">
                    <form method="get" action="{{ route('motorbikes.search') }}">
                        <div class="input-group">
                            <input class="form-control" name="search" placeholder="Search ..." value="{{ $search ?? '' }}"
                                style="
                            border: 1px solid #ccc;
                            padding: 10px;
                            border-top-right-radius: 0 !important;
                            border-bottom-right-radius: 0px !important;">
                            <button type="submit" class="btn btn-primary"
                                style="margin-left: 0px;margin-bottom: 0px;">Search</button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="table-responsive">
                <table class= "table table-hover" style="vertical-align: middle; border: 1px solid #134ca1;">
                    <tr class="table-primary">
                        <th>#</th>
                        <th>Tên xe</th>
                        <th>Hãng xe</th>
                        <th>Biển số</th>
                        <th>Giá thuê</th>
                        <th>Loại xe</th>
                        <th>Trạng thái</th>
                        <th>Hình ảnh</th>
                        <th>Hành động</th>
                    </tr>
                    @php
                        $index = 1;
                    @endphp
                    @foreach ($motorbikes as $motorbike)
                        <tr>
                            <td>{{ $index }}</td>
                            <td>{{ $motorbike->tenxe }}</td>
                            <td>{{ $motorbike->hangxe }}</td>
                            <td>{{ $motorbike->bienso }}</td>
                            <td>{{ $motorbike->giathue }}</td>
                            <td>{{ $motorbike->classify->tenphanloai }}</td>
                            <td>{{ $motorbike->status }}</td>
                            <td>
                                <a href="{{ asset('../productImg/' . $motorbike->image) }}" data-fancybox data-caption="Hello">
                                    <img src="{{ asset('../productImg/' . $motorbike->image) }}" alt="" width="200px"
                                        height="120px">
                                </a>
                            </td>
                            <td>
                                <a href="{{ route('motorbikes.edit', $motorbike->id) }}" style="text-decoration: none;">
                                    <i style="color: black;" class="fas fa-pen"></i>
                                </a>

                                <form action="{{ route('motorbikes.destroy', $motorbike->id) }}" id="form-delete{{ $motorbike->id }}"
                                    method="post" class="d-inline" style="margin:0;">
                                    @csrf
                                    @method('delete')
                                    <button class="btn btn-link btn-delete" data-id={{ $motorbike->id }} style="margin:0;">
                                        <i style="color: red;" class="fas fa-trash"></i></button>
                                </form>

                            </td>
                        </tr>
                        @php
                            $index++;
                        @endphp
                    @endforeach

                </table>
            </div>
            {{ $motorbikes->links() }}
        </div>

    </div>

@endsection
@push('script')
    <script>
        @if (session('message'))
            // alert('{{ session('message') }}');
            Swal.fire({
                position: "top-end",
                icon: '{{ session('statuscode') }}',
                title: '{{ session('message') }}',
                showConfirmButton: false,
                timer: 1500,
            })
        @endif
    </script>
@endpush
