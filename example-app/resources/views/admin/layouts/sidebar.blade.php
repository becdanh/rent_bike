<aside
    class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-3   bg-gradient-dark"
    id="sidenav-main">
    <div class="sidenav-header">
        <i class="fas fa-times p-3 cursor-pointer text-white opacity-5 position-absolute end-0 top-0 d-none d-xl-none"
            aria-hidden="true" id="iconSidenav"></i>
        <a class="navbar-brand m-0" href="{{ route('dashboard') }}">
            <img src="https://static.vecteezy.com/system/resources/previews/009/117/067/non_2x/ddn-logo-ddn-letter-ddn-letter-logo-design-initials-ddn-logo-linked-with-circle-and-uppercase-monogram-logo-ddn-typography-for-technology-business-and-real-estate-brand-vector.jpg" class="navbar-brand-img h-100" alt="main_logo">
            <span class="ms-1 font-weight-bold text-white">DDN Motorbike Rental</span> <br>
            {{-- <span class="ms-1 font-weight-bold text-white">Hello: {{ auth()->user()->name }}</span> --}}
        </a>
    </div>
    <hr class="horizontal light mt-0 mb-2">
    <div class="collapse navbar-collapse  w-auto " id="sidenav-collapse-main">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link text-white {{ request()->routeIs('dashboard') ? 'bg-gradient-info active' : ''}}" href="{{ route('dashboard') }}">
                    <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="material-icons opacity-10">dashboard</i>
                    </div>
                    <span class="nav-link-text ms-1">Dashboard</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white {{ request()->routeIs('motorbikes.*') ? 'bg-gradient-info active' : ''}} " href="{{ route('motorbikes.index') }}">
                    <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="material-icons opacity-10">motorcycle</i>
                    </div>
                    <span class="nav-link-text ms-1">Motorbike</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white {{ request()->routeIs('customers.*') ? 'bg-gradient-info active' : ''}} " href="{{ route('customers.index') }}">
                    <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="material-icons opacity-10">person</i>
                    </div>
                    <span class="nav-link-text ms-1">Customer</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white {{ request()->routeIs('classify.*') ? 'bg-gradient-info active' : ''}} " href="{{route('classify.index') }}">
                    <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="material-icons opacity-10">view_in_ar</i>
                    </div>
                    <span class="nav-link-text ms-1">Classify</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white {{ request()->routeIs('orderbikes.*')  ? 'bg-gradient-info active' : ''}} " href="{{ route('orderbikes.index') }}">
                    <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="material-icons opacity-10">handshake</i>
                    </div>
                    <span class="nav-link-text ms-1">Orderbikes</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white {{ request()->routeIs('bills.*')  ? 'bg-gradient-info active' : ''}}" href="{{ route('bills.index') }}">
                    <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="material-icons opacity-10">local_grocery_store</i>
                    </div>
                    <span class="nav-link-text ms-1">Bills</span>
                </a>
            </li>

        </ul>
    </div>
</aside>
