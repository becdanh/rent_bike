@extends('admin.layouts.app')
@section('title', 'create classify')
@section('content')
    <div class="card">
        <h1>Thêm Mới</h1>
        <div>
            <form action="{{route('classify.store')}}" method="POST">
                @csrf
    
                <div class="input-group input-group-static mb-4">
                    <label>Tên phân loại</label>
                        <input type="text" value="{{ old('tenphanloai') }}" name="tenphanloai" class="form-control">
    
                        @error('tenphanloai')
                            <span class="text-danger"> {{ $message }}</span>
                        @enderror
                </div>
        </div>

            <button type="submit" class="btn btn-submit btn-primary">Thêm mới</button>
        </form>
    </div>

@endsection
