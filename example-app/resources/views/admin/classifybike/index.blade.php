@extends('admin.layouts.app')
@section('title', 'Classifybikes')
@section('content')
    <div class="card">

        <h1>
            Danh sách phân loại xe
        </h1>

        <div>
            <a href="{{ route('classify.create') }}" class="btn btn-primary" style="background-color:green">Create</a>
        </div>

        <div class="table-responsive">
            <table class="table table-hover" style="vertical-align: middle; border: 1px solid #134ca1;">
                <tr class="table-primary">
                    <th>STT</th>
                    <th>Tên phân loại</th>
                    <th>Hành động</th>
                </tr>
                @php
                    $index = 1;
                @endphp
                @foreach ($classifys as $classify)
                    <tr>
                        <td>{{ $index }}</td>
                        <td>{{ $classify->tenphanloai }}</td>
                        <td>
                            <a href="{{ route('classify.edit', $classify->id) }}" style="text-decoration: none;">
                                <i style="color: black;" class="fas fa-pen"></i>
                            </a>

                            <form action="{{ route('classify.destroy', $classify->id) }}"
                                id="form-delete{{ $classify->id }}" method="post" style="margin: 0;" class="d-inline">
                                @csrf
                                @method('delete')
                                <button class="btn btn-link btn-delete" data-id={{ $classify->id }} style="margin: 0;">
                                    <i class="fas fa-trash" style="color: red;"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                    @php
                        $index++;
                    @endphp
                @endforeach

            </table>
            {{ $classifys->links() }}
        </div>
    </div>
@endsection

@push('script')
    <script>
        @if (session('message'))
            // alert('{{ session('message') }}');
            Swal.fire({
                position: "top-end",
                icon: '{{ session('statuscode') }}',
                title: '{{ session('message') }}',
                showConfirmButton: false,
                timer: 1500,
            })
        @endif
    </script>
@endpush
