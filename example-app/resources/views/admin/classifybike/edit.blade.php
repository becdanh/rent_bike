@extends('admin.layouts.app')
@section('title', 'edit classify'. $classifys->tenphanloai)
@section('content')
    <div class="card">
        <h1>Chỉnh sửa</h1>
    </div>

    <div></div>
        <form action="{{route('classify.update', $classifys->id)}}" method="POST">
            @csrf
            @method('PUT')
            <div class="input-group input-group-static mb-4">
                <label>Tên phân loại</label>
                    <input type="text" value="{{ old('tenphanloai') }}" name="tenphanloai" class="form-control">

                    @error('tenphanloai')
                        <span class="text-danger"> {{ $message }}</span>
                    @enderror
            </div>
            <button type="submit" class="btn btn-submit btn-primary">Cập nhập</button>
        </form>
    </div>

@endsection
