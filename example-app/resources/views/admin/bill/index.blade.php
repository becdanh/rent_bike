@extends('admin.layouts.app')
@section('title', 'Bills')
@section('content')
    <div class="card" style="padding: 32px;">
        <h1>
            Danh sách hóa đơn
        </h1>

        <div>
            <div class="row">
                <div class="col-md-6">
                    <form method="get" action="{{ route('bills.search') }}" style="margin-bottom: 12px;">
                        <div class="input-group">
                            <input class="form-control" name="search" placeholder="Search ..."
                                style="
                            border: 1px solid #ccc;
                            padding: 10px;
                            border-top-right-radius: 0 !important;
                            border-bottom-right-radius: 0px !important;"
                                value="{{ $search ?? '' }}">
                            <button type="submit" class="btn btn-primary"
                                style="margin-left: 0px;margin-bottom: 0px;">Search</button>
                        </div>
                    </form>
                </div>
                <div class="table-responsive">
                    <table class="table table-hover" style="vertical-align: middle; border: 1px solid #134ca1;">
                        <tr class="table-primary">
                            <th>STT</th>
                            <th>Tên khách hàng</th>
                            <th>Tên xe</th>
                            <th>Biển số</th>
                            <th>Giá thuê</th>
                            <th>Ngày bắt đầu thuê</th>
                            <th>Ngày trả</th>
                            <th>Tạm ứng</th>
                            <th>Phụ thu</th>
                            <th>Ghi chú</th>
                            <th>Tổng tiền</th>
                        </tr>
                        @php
                            $index = 1;
                        @endphp

                        @foreach ($bills as $bill)
                            <tr>
                                <td>{{ $index }}</td>
                                <td>{{ $bill->orderbike->customer->hoten }}</td>
                                <td>{{ $bill->orderbike->orderbike_bike->tenxe }}</td>
                                <td>{{ $bill->orderbike->orderbike_bike->bienso }}</td>
                                <td>{{ $bill->orderbike->orderbike_bike->giathue }}</td>
                                <td>{{ $bill->orderbike->ngaybatdauthue }}</td>
                                <td>{{ $bill->ngaytra }}</td>
                                <td>{{ $bill->orderbike->tamung }} </td>
                                <td>{{ $bill->phuthu }}</td>
                                <td>{{ $bill->ghichu }}</td>
                                <td>{{ $bill->tongtien }}</td>
                            </tr>
                            @php
                                $index++;
                            @endphp
                        @endforeach

                    </table>
                    {{ $bills->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        @if (session('message'))
            // alert('{{ session('message') }}');
            Swal.fire({
                position: "top-end",
                icon: '{{ session('statuscode') }}',
                title: '{{ session('message') }}',
                showConfirmButton: false,
                timer: 1500,
            })
        @endif
    </script>
@endpush
