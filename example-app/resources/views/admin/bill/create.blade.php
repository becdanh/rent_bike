@extends('admin.layouts.app')
@section('title', 'Create Orderbikes')
@section('content')
    <div class="card" style="padding: 32px;">
        <h1>Thanh toán</h1>


        @php
            date_default_timezone_set('Asia/Ho_Chi_Minh');
            $currentDateTime = date('Y-m-d\TH:i');
            $ngayBatDauThue = $orderbike->ngaybatdauthue;
            $ngayTra = old('ngaytra', $currentDateTime);
            // Chuyển chuỗi ngày giờ sang dạng đối tượng Datetime
            $dateTimeBatDau = new DateTime($ngayBatDauThue);
            $dateTimeTra = new DateTime($ngayTra);
            
            // Tính số ngày thuê
            $soNgayThue = $dateTimeBatDau->diff($dateTimeTra)->days;
            $soGio = $dateTimeBatDau->diff($dateTimeTra)->h;
            $soPhut = $dateTimeBatDau->diff($dateTimeTra)->i;
            $tienTamUng = (int) $orderbike->tamung;
            $tienPhatSinhMoiGio = 20000;
            if ($soGio >= 8) {
                $soNgayThue++;
                $soGio = 0;
            }
            
            if ($soPhut >= 30) {
                $soGio++;
                $soPhut = 0;
            }
            
            // Tính tổng tiền thuê dựa trên giá thuê và số ngày thuê
            $giaThue = $motorbike->giathue;
            $tongTien = $giaThue * $soNgayThue + $soGio * $tienPhatSinhMoiGio - $tienTamUng;
        @endphp


<form action="{{ route('bills.store') }}" method="POST">
    @csrf
    <div class="col-md-4">
        <label for="">OrderbikeId</label>
        <input class="p-2 border" name="orderbike_id" value="{{ $orderbike->id }}">
    </div>
    <div class="col-md-4">
        <label for="">Tên khách hàng</label>
        <div class="p-2 border">{{ $customer->hoten }}</div>
    </div>

    <div class="col-md-4">
        <label for="">Tên xe</label>
        <div class="p-2 border">{{ $motorbike->tenxe }} - {{ $motorbike->bienso }}</div>
    </div>

    <div class="col-md-4">
        <label for="">Giá thuê</label>
        <div class="p-2 border">{{ $motorbike->giathue }}</div>
    </div>

    <div class="col-md-4">
        <label for="">Ngày bắt đầu thuê</label>
        <div class="p-2 border">{{ $orderbike->ngaybatdauthue }}</div>
    </div>

    <div class="input-group input-group-static mb-4">
        <label name="group" class="ms-0">Ngày trả</label>
        @php
        // ... (Mã PHP lấy ngày và giờ hiện tại)
        @endphp
        <input type="datetime-local" class="form-control" value="{{ old('ngaytra', $currentDateTime) }}" name="ngaytra" placeholder="Ngày trả">
    </div>

    <div class="col-md-4">
        <label for="">Tạm ứng</label>
        <div class="p-2 border">{{ $orderbike->tamung }}</div>
    </div>

    <div class="col-md-4">
        <label for="">Phụ thu</label>
        <input type="text" class="form-control" value="{{ old('phuthu','0') }}" name="phuthu" id="phuthu" placeholder="">
        @error('phuthu')
            <span class="text-danger">{{ $message }}</span>
        @enderror
    </div>

    <div class="col-md-4">
        <label for="">Ghi chú</label>
        <input type="text" class="form-control" value="{{ old('ghichu') }}" name="ghichu" placeholder="Ghi chú">
        @error('ghichu')
            <span class="text-danger">{{ $message }}</span>
        @enderror
    </div>

    <div class="col-md-4">
        <label for="">Tổng tiền</label>
        <input class="p-2 border" name="tongtien" id="tongtien" value="{{ $tongTien }}">
    </div>

    <button type="submit" class="btn btn-submit btn-primary">Thêm mới</button>
</form>
</div>

<script>
// Lắng nghe sự kiện mất focus (blur) của trường phụ thu
document.getElementById('phuthu').addEventListener('blur', function() {
    // Lấy giá trị phụ thu và tổng tiền hiện tại
    var phuThu = parseFloat(this.value);
    var tongTienHienTai = parseFloat(document.getElementById('tongtien').value);

    // Tính tổng tiền mới
    var tongTienMoi = tongTienHienTai + phuThu;

    // Cập nhật giá trị tổng tiền mới
    document.getElementById('tongtien').value = tongTienMoi.toFixed(2);
});
</script>
@endsection
