@extends('admin.layouts.app')
@section('title','customers')
@section('content')
    <div class="card" style="padding: 32px;">


        <h1>
            Danh sách khách hàng
        </h1>

        <div>

            <div class="row">
                <div class="col-md-6">
                    <a href="{{ route('customers.create') }}" class="btn btn-primary" style="background-color:green">Create</a>
                </div>
                <div class="col-md-6">
                    <form method="get" action="{{ route('customers.search') }}">
                        <div class="input-group">
                            <input class="form-control" name="search" placeholder="Search ..."
                            style="
                            border: 1px solid #ccc;
                            padding: 10px;
                            border-top-right-radius: 0 !important;
                            border-bottom-right-radius: 0px !important;">
                            <button type="submit" class="btn btn-primary" style="margin-left: 0px;margin-bottom: 0px;">Search</button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="table-responsive">
                <table class= "table table-hover" style="vertical-align: middle; border: 1px solid #134ca1;">
                    <tr class="table-primary">
                        <th>STT</th>
                        <th>Họ và tên</th>
                        <th>Số điện thoại</th>
                        <th>Căn cước công dân</th>
                        <th>Bằng lái</th>
                        <th>Ảnh mặt trước</th>
                        <th>Ảnh mặt sau</th>
                        <th>Hành động</th>
                    </tr>
                    @php
                        $index = 1;
                    @endphp
                    @foreach ($customers as $customer)
                    <tr>
                        <td>{{ $index }}</td>
                        <td>{{ $customer->hoten }}</td>
                        <td>{{ $customer->sdt }}</td>
                        <td>{{ $customer->cccd }}</td>
                        <td>{{ $customer->banglai }}</td>
                        <td> <a href="{{asset('../imagecustomers/'.$customer->cccd1) }}" data-fancybox data-caption="Single image"><img src="{{asset('../imagecustomers/'.$customer->cccd1) }}" width="150px" height="100px"></a></td>
                        <td> <a href="{{asset('../imagecustomers/'.$customer->cccd2) }}" data-fancybox data-caption="Single image"><img src="{{asset('../imagecustomers/'.$customer->cccd2) }}" width="150px" height="100px"></td>
                        <td>
                            <a href="{{ route('customers.edit', $customer->id) }}" style="text-decoration: none;">
                                <i style="color: black;" class="fas fa-pen"></i>
                            </a>

                            <form action="{{ route('customers.destroy', $customer->id) }}" id="form-delete{{ $customer->id }}"
                                method="post" style="margin: 0;" class="d-inline">
                                @csrf
                                @method('delete')
                                <button class="btn btn-link btn-delete" data-id={{ $customer->id }} style="margin: 0;">
                                    <i class="fas fa-trash" style="color: red;"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                    @php
                        $index++;
                    @endphp
                    @endforeach
                </table>
            </div>
                {{ $customers->links() }}
        </div>
    </div>

@endsection

@push('script')
    <script>
        @if (session('message'))
            // alert('{{ session('message') }}');
            Swal.fire({
                position: "top-end",
                icon: '{{ session('statuscode') }}',
                title: '{{ session('message') }}',
                showConfirmButton: false,
                timer: 1500,
            })
        @endif
    </script>
@endpush
