@extends('admin.layouts.app')
@section('title', 'Create customers')
@section('content')
    <div class="card">
        <h1>Thêm khách hàng mới</h1>
        <div>
            <form action="{{ route('customers.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="input-group input-group-static mb-4">
                    <label>Họ và tên</label>
                    <input type="text" class="form-control" value="{{ old('hoten') }}" name="hoten"
                        placeholder="Họ và tên">

                    @error('hoten')
                        <span class="text-danger"> {{ $message }}</span>
                    @enderror

                </div>

                <div class="input-group input-group-static mb-4">
                    <label>Số điện thoại</label>
                    <input type="text" class="form-control" value="{{ old('sdt') }}" name="sdt"
                        placeholder="Số điện thoại">

                    @error('sdt')
                        <span class="text-danger"> {{ $message }}</span>
                    @enderror

                </div>

                <div class="input-group input-group-static mb-4">
                    <label>Căn cước công dân</label>
                    <input type="text" class="form-control" value="{{ old('cccd') }}" name="cccd"
                        placeholder="Căn cước công dân">

                    @error('cccd')
                        <span class="text-danger"> {{ $message }}</span>
                    @enderror

                </div>

                <div class="input-group input-group-static mb-4">
                    <label>Bằng lái</label>
                    <input type="text" class="form-control" value="{{ old('banglai') }}" name="banglai"
                        placeholder="Bằng lái">

                    @error('banglai')
                        <span class="text-danger"> {{ $message }}</span>
                    @enderror

                </div>

                <div class="row">
                    <div class="input-group-static col-5 mb-4">
                        <label>Ảnh mặt trước</label>
                        <input type="file" class="form-control" value="{{ old('cccd1') }}" name="cccd1"
                            id="image-input" placeholder="thêm ảnh">

                        @error('cccd1')
                            <span class="text-danger"> {{ $message }}</span>
                        @enderror
                    </div>
                    <div class="col-5">
                        <img src="{{ asset('../imagecustomers/no-image.png') }}" id="show-image" alt=""
                            width="200" height="140">
                    </div>
                </div>


                <div class="row">
                    <div class="input-group-static col-5 mb-4">
                        <label>Ảnh mặt sau</label>
                        <input type="file" class="form-control" value="{{ old('cccd2') }}" name="cccd2"
                            id="image-input1" placeholder="thêm ảnh">

                        @error('cccd2')
                            <span class="text-danger"> {{ $message }}</span>
                        @enderror
                    </div>
                    <div class="col-5">
                        <img src="{{ asset('../imagecustomers/no-image.png') }}" id="show-image1" alt=""
                            width="200" height="140">
                    </div>
                </div>

                <button type="submit" class="btn btn-submit btn-primary">Thêm mới</button>
            </form>
        </div>
    </div>

@endsection





<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
