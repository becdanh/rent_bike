<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Orderbike\CreateOrderRequest;
use App\Http\Requests\Orderbike\UpdateOrderRequest;
use App\Models\Customer;
use App\Models\Motorbike;
use App\Models\Orderbike;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class OrderbikeController extends Controller
{   

    protected $orderbike;

    /**
     * @pram $bike
     */
    public function __construct(Orderbike $orderbike) {
        $this->orderbike = $orderbike;
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {   
        $orderbikes = Orderbike::with('customer','orderbike_bike')
        ->paginate(15);
            // dd($orderbikes);
        return view('admin.orderbike.index',compact('orderbikes'));
        
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {   
        $customers = Customer::all();
        $motorbikes = Motorbike::where('status', 'Còn trống')->get();
        if ($motorbikes->isEmpty()) {
            Session::flash('statuscode','error');
            return redirect()->route('orderbikes.index')->with('message', 'Không còn xe nào đang trống!');
        }
        return view('admin.orderbike.create',compact('customers','motorbikes'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CreateOrderRequest $request)
    {   
        $orderData = $request->all();
    if ($request->has('action') && $request->input('action') === 'thuexe') {
        $orderData['status'] = 'Đã giao xe';
        $motorbikeId = $orderData['motorbike_id'];
        Motorbike::where('id', $motorbikeId)->update(['status' => 'Đang bận']);
    } else {
        $orderData['status'] = 'Chưa giao xe';
        $motorbikeId = $orderData['motorbike_id'];
        Motorbike::where('id', $motorbikeId)->update(['status' => 'Đang bận']);
    }
    Orderbike::create($orderData);
        // Orderbike::create($request->all());
        
        // $motorbikeId = $request['motorbike_id'];
        // Motorbike::where('id', $motorbikeId)->update(['status' => 'Đang bận']);
        Session::flash('statuscode','success');
        return to_route('orderbikes.index')->with(['message'=>'Cho thuê thành công']);
        
        
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $orderbikes = Orderbike::with('customer', 'orderbike_bike')->findOrFail($id);
        return view('admin.orderbike.show', compact('orderbikes'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {   
        $orderbikes = Orderbike::findOrFail($id);
        $customers = Customer::all();
        // $motorbikes = Motorbike::all();
        $motorbikeOld = Motorbike::findOrFail($orderbikes->orderbike_bike->id);
        $motorbikes = Motorbike::where('motorbikes.status', 'Còn trống')->get();
        return view('admin.orderbike.edit',compact('customers','motorbikes','orderbikes', 'motorbikeOld'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateOrderRequest $request, string $id)
    {   
        // dd(123);
        $data = $request->all();
        // $request['status'] = '123123';
        $orderbikes = Orderbike::findOrFail($id);

        $motorbikeOldId = $orderbikes->motorbike_id;
        $motorbikeNewId = $data['motorbike_id'];

        if ($motorbikeOldId !== $motorbikeNewId) {
            $motorbikeOld = Motorbike::findOrFail($motorbikeOldId);
            $motorbikeOld->update(['status' => 'Còn trống']);
        }

        $motorbikeNew = Motorbike::findOrFail($motorbikeNewId);
        $motorbikeNew->update(['status' => 'Đang bận']);
        // dd($request->all());
        $orderbikes->update($data);

        Session::flash('statuscode','success');
        return to_route('orderbikes.index')->with(['message'=>'Cập nhật thành công']);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        // Orderbike::destroy($id);
        // return to_route('orderbikes.index')->with(['message'=>'Xóa thành công']);
        $orderbike = Orderbike::findOrFail($id);

        // Check if the order has the status "Chưa giao xe" before allowing deletion
        if ($orderbike->status === 'Chưa giao xe') {
            // Update bike status to "Còn trống"
            $orderbike->orderbike_bike->update(['status' => 'Còn trống']);
    
            // Delete the order
            $orderbike->delete();
            Session::flash('statuscode','success');
            return to_route('orderbikes.index')->with(['message' => 'Xóa thành công']);
        } else {
            Session::flash('statuscode','error');
            return to_route('orderbikes.index')->with(['message' => 'Không thể xóa đơn hàng đã giao xe']);
        }
    }

    public function search(Request $request)
    {
        $search = $request->search;
        $orderbikes = Orderbike::where(function($query) use ($search){
            $query->where('id',"$search")
            ->orWhere('status','like',"%$search%")
            ->orWhere('ngaybatdauthue','like',"%$search%")
            ->orWhere('ngaydutra','like',"%$search%");
        })
        ->orWhereHas('customer',function($query) use($search){
            $query->where('hoten','like',"%$search%");
        })
        ->orWhereHas('orderbike_bike',function($query) use($search){
            $query->where('tenxe','like',"%$search%")
            ->orWhere('bienso','like',"%$search%")
            ->orWhere('giathue','like',"%$search%");
        })
        ->paginate(5)
        ->appends(['search' => $search]);
        return view('admin.orderbike.index', compact('orderbikes', 'search'));
    }
}
