<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Motorbike\CreateMotorbikeRequest;
use App\Http\Requests\Motorbike\UpdateMotorbikeRequest;
use App\Models\Classifybike;
use App\Models\Motorbike;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class MotorbikeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        $motorbikes = Motorbike::with('classify')->paginate(10);
        return view('admin.motorbikes.index', compact('motorbikes'));
    }
    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $classifys = Classifybike::all();
        return view('admin.motorbikes.create',compact('classifys'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CreateMotorbikeRequest $request)
    {
        if($request->has('file_upload')) {
            $file = $request ->file_upload;
            $ext = $request ->file_upload->extension();
            $file_name = rand(1,10000).'-'.'product.'.$ext;
            $file->move(public_path('productImg'),$file_name);
            $request->merge(['image' => $file_name]);
        }
        $request['status'] = 'Còn trống';
        Motorbike::create($request->all());
        Session::flash('statuscode','success');
        return to_route('motorbikes.index')->with(['message'=>'Thêm mới thành công']);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $classifys = Classifybike::all();
        $motorbikes = Motorbike::findOrFail($id);
        return view('admin.motorbikes.edit',compact('motorbikes','classifys'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateMotorbikeRequest $request, $id)
    {
        $motorbikes = Motorbike::findOrFail($id);
        if($request->has('file_upload')) {
                unlink('productImg/' .$motorbikes->image);
                $file = $request ->file_upload;
                $ext = $file->extension();
                $file_name = rand(1,10000).'-'.'product.'.$ext;
                $file->move(public_path('productImg'),$file_name);
                $request->merge(['image' => $file_name]);
            }
        $motorbikes->update( $request->all());
        Session::flash('statuscode','success');
        return to_route('motorbikes.index')->with(['message'=>'Cập nhật thành công']);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $motorbikes= Motorbike::find($id);
        $destination = 'productImg/' .$motorbikes->image;
        if(file_exists($destination)){
            unlink($destination);
        }
        $motorbikes->delete();
        Session::flash('statuscode','success');
        return to_route('motorbikes.index')->with(['message'=>'Xóa thành công']);
    }

    public function search(Request $request)
    {
        $search = $request->search;
        $motorbikes = Motorbike::where(function($query) use ($search){
            $query->where('id',"$search")
            ->orWhere('tenxe','like',"%$search%")
            ->orWhere('hangxe','like',"%$search%")
            ->orWhere('bienso','like',"%$search%")
            ->orWhere('giathue','like',"%$search%");
        })
        ->orWhereHas('classify',function($query) use($search){
            $query->where('tenphanloai','like',"%$search%");
        })
        ->paginate(5)
        ->appends(['search' => $search]);
        return view('admin.motorbikes.index', compact('motorbikes', 'search'));
    }

}
