<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Bill;
use App\Models\Customer;
use App\Models\Motorbike;
use App\Models\Orderbike;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    protected $motorbike;
    protected $customer;
    protected $orderbike;
    protected $bill;

    public function __construct(Motorbike $motorbike, Customer $customer, Orderbike $orderbike, Bill $bill)
    {
        $this->motorbike = $motorbike;
        $this->customer = $customer;
        $this->orderbike = $orderbike;
        $this->bill = $bill;
    }

    public function index(){

        $motorbikeCount = $this->motorbike->count();
        $customerCount = $this->customer->count();
        $orderbikeCount = $this->orderbike->count();
        $billCount = $this->bill->count();
        return view('admin.dashboard.index', compact('motorbikeCount',
         'customerCount','orderbikeCount','billCount'));
    }
}
