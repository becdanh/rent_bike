<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Customers\CreateCustomerRequest;
use App\Http\Requests\Customers\UpdateCustomerRequest;
use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $customers = Customer::latest('id')->paginate(3);
        return view('admin.customer.index',compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.customer.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CreateCustomerRequest $request)
    {
         $datacreate = $request->all();
         //dd($request->all());
        if ($request->hasFile('cccd1'))
        {
            $file = $request->cccd1;
            //$ext = $request->image->extension();
            //$file_name =time().'.'.$ext;
            $file_name = $file->getClientOriginalName();
            $file->move(public_path('imagecustomers'),$file_name);
            $datacreate['cccd1'] = $file_name;
        }
        if ($request->hasFile('cccd2'))
        {
            $file1 = $request->cccd2;
            $file_name1 = $file1->getClientOriginalName();
            $file1->move(public_path('imagecustomers'),$file_name1);
            $datacreate['cccd2'] = $file_name1;
        }
        Customer::create($datacreate);
        Session::flash('statuscode','success');
        return to_route('customers.index')->with(['message'=>'Thêm mới thành công']);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $customer = Customer::findOrFail($id);
        return view('admin.customer.edit',compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateCustomerRequest $request, string $id)
    {
        $customer = Customer::findOrFail($id);

        $data = $request->all();
        if ($request->hasFile('cccd1'))
        {    unlink('imagecustomers/' .$customer->cccd1);
            $file = $request->cccd1;
            //$ext = $request->image->extension();
            //$file_name =time().'.'.$ext;
            $file_name = $file->getClientOriginalName();
            $file->move(public_path('imagecustomers'),$file_name);
            $datacreate['cccd1'] = $file_name;
        }
        else {
            // Giữ lại ảnh cũ nếu không có ảnh mới được tải lên
            $datacreate['cccd1'] = $customer->cccd1;
        }
        if ($request->hasFile('cccd2'))
        {   unlink('imagecustomers/' .$customer->cccd2);
            $file1 = $request->cccd2;
            $file_name1 = $file1->getClientOriginalName();
            $file1->move(public_path('imagecustomers'),$file_name1);
            $datacreate['cccd2'] = $file_name1;
        }
        else {
            // Giữ lại ảnh cũ nếu không có ảnh mới được tải lên
            $datacreate['cccd2'] = $customer->cccd2;
        }

        $customer->update($datacreate);
        Session::flash('statuscode','success');
        return to_route('customers.index')->with(['message'=>'Cập nhật thành công']);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $customers =Customer::find($id);
        $destinationPath = 'imagecustomers/'.$customers->cccd1;
        $destinationPaths = 'imagecustomers/'.$customers->cccd2;
        if(file_exists($destinationPath )){
            unlink($destinationPath);
        }
        if(file_exists($destinationPaths )){
            unlink($destinationPaths);
        }
        $customers->delete();
       // Customer::destroy($id);
       Session::flash('statuscode','success');
        return to_route('customers.index')->with(['message'=>'Xóa thành công']);

    }

public function search(Request $request)
{

    // Logic tìm kiếm
    $search = $request->search;
    $customers = Customer::where(function($query) use ($search){
        $query->where('hoten','like',"%$search%")
        ->orWhere('id',$search)
        ->orWhere('banglai','like',"%$search%")
        ->orWhere('cccd','like',"%$search%")
        ->orWhere('sdt','like',"%$search%");
    })
    ->paginate(3)
    ->appends(['search' => $search]);
    return view('admin.customer.index', compact('customers', 'search'));
}









































public function storeAjax(CreateCustomerRequest $request)
{
    // dd(123);
    // Create a new customer record and save it to the database
    // $datacreate = $request->all();
    // Customer::create($datacreate);
    // return to_route('customers.index')->with(['message'=>'Thêm mới thành công']);

    // $validatedData = $request->validate([
    //     'hoten' => 'required|max:255',
    //     'sdt' => 'required|max:15',
    //     // Thêm các quy tắc hợp lệ cho các trường dữ liệu khác tương tự ở đây
    // ]);
    $customer = new Customer();
    $customer->hoten = $request->input('hoten');
    $customer->sdt = $request->input('sdt');
    $customer->cccd = $request->input('cccd');
    $customer->banglai = $request->input('banglai');
    // Lưu ảnh mặt trước căn cước công dân
    if ($request->hasFile('cccd1')) {
        $file = $request->file('cccd1');
        $file_name = $file->getClientOriginalName();
        $file->move(public_path('imagecustomers'), $file_name);
        $customer->cccd1 = $file_name;
    }

    // Lưu ảnh mặt sau căn cước công dân
    if ($request->hasFile('cccd2')) {
        $file1 = $request->file('cccd2');
        $file_name1 = $file1->getClientOriginalName();
        $file1->move(public_path('imagecustomers'), $file_name1);
        $customer->cccd2 = $file_name1;
    }
    // Thêm các trường dữ liệu khác tương tự ở đây (nếu có)

    // Lưu thông tin khách hàng vào cơ sở dữ liệu
    $customer->save();
    Session::flash('statuscode','error');
     return response()->json([
        'data' => $customer,
        'message'=>'Thêm mới khách hàng thành công'
    ], 200);
    return to_route('orderbikes.index')->with(['message'=>'Thêm mới khách hàng thành công']);
}
}
