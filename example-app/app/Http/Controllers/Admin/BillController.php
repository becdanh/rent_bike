<?php

namespace App\Http\Controllers\Admin;

use App\Models\Bill;
use App\Models\Orderbike;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class BillController extends Controller
{
    protected $bill;

    /**
     * @pram $bike
     */
    public function __construct(Bill $orderbike) {
        $this->bill = $orderbike;
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $bills = Bill::with('orderbike')

        ->paginate(15);

        return view('admin.bill.index',compact('bills'));

    }
    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request)
    {
        $orderbikeId = $request->query('orderbike');
        $tamung = $request->query('tamung');
        $ngaybatdauthue = $request->query('ngaybatdauthue');

        $orderbike = Orderbike::findOrFail($orderbikeId);
        $customer = $orderbike->customer;

        $motorbike = $orderbike->orderbike_bike;

        // Pass the data to the bill creation view
        return view('admin.bill.create', compact('customer', 'motorbike', 'orderbike', 'tamung', 'ngaybatdauthue'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

        // Create the Bill record
        Bill::create($request->all());

        // Get the orderbike_id from the request
        $orderbikeId = $request->input('orderbike_id');

        // Update the status of the Orderbike to 'Đã thanh toán'
        $orderbike = Orderbike::find($orderbikeId);
        if ($orderbike) {
            $orderbike->status = 'Đã thanh toán';
            $orderbike->save();
        }

        // Update the status of the Motorbike to 'Còn trống'
        $motorbike = $orderbike->orderbike_bike;
        if ($motorbike) {
            $motorbike->status = 'Còn trống';
            $motorbike->save();
        }

        //Bill::create($request->all());
        Session::flash('statuscode','success');
        return redirect()->route('bills.index')->with(['message' => 'Thêm mới thành công']);
    }


    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    public function search(Request $request)
    {

        $search = $request->search;
        $bills = Bill::where(function($query) use ($search){
        $query->where('id', "$search")
            ->orWhere('ngaytra', 'like', "%$search%")
            ->orWhereHas('orderbike.customer', function ($query) use ($search) {
                $query->where('hoten', 'like', "%$search%");
            })
            ->orWhereHas('orderbike.orderbike_bike', function ($query) use ($search) {
                $query->where('tenxe', 'like', "%$search%")
                    ->orWhere('bienso', 'like', "%$search%")
                    ->orWhere('giathue', 'like', "%$search%");
            });
    })
    ->paginate(5)
    ->appends(['search' => $search]);



        return view('admin.bill.index', compact('bills', 'search'));

        }



}
