<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Classify\CreateClassifyRequest;
use App\Models\Classifybike;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ClassifyController extends Controller
{


    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $classifys = Classifybike::paginate(3);
        return view('admin.classifybike.index', compact('classifys'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.classifybike.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CreateClassifyRequest $request)
    {
        $datacreate = $request->all();
        Classifybike::create($datacreate);
        Session::flash('statuscode','success');
        return to_route('classify.index')->with(['message'=>'Thêm mới thành công']);

    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $classifys = Classifybike::findOrFail($id);
        return view('admin.classifybike.edit',compact('classifys'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $customer = Classifybike::findOrFail($id);

        $data = $request->all();

        $customer->update($data);
        Session::flash('statuscode','success');
        return to_route('classify.index')->with(['message'=>'Cập nhật thành công']);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        Classifybike::destroy($id);
        Session::flash('statuscode','success');
        return to_route('classify.index')->with(['message'=>'Xóa thành công']);
    }

    public function search(Request $request)
    {

    // Logic tìm kiếm
    $search = $request->search;

    $classifys = Classifybike::where(function($query) use ($search){
        $query->Where('id',$search)
        ->orWhere('tenphanloai','like',"%$search%");

    })
    ->paginate(3)
    ->appends(['search' => $search]);
    return view('admin.classifybike.index', compact('classifys', 'search'));

    }

}
