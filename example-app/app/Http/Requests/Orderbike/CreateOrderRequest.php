<?php

namespace App\Http\Requests\Orderbike;

use Illuminate\Foundation\Http\FormRequest;

class CreateOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'customer_id' => 'required|exists:customers,id',
            'motorbike_id' => 'required|exists:motorbikes,id',
            'ngaybatdauthue' => 'date',
            'ngaydutra' => 'date|after_or_equal:ngaybatdauthue',
            'tamung' => 'numeric|min:0',
            'ghichu' => 'nullable'
        ];
    }
        public function messages(): array
    {
        return [
            'customer_id.required' => 'Vui lòng nhập ID khách hàng.',
            'customer_id.exists' => 'Khách hàng không tồn tại trong cơ sở dữ liệu.',
            'motorbike_id.required' => 'Vui lòng nhập ID xe máy.',
            'motorbike_id.exists' => 'Xe máy không tồn tại trong cơ sở dữ liệu.',
            'ngaybatdauthue.date' => 'Ngày bắt đầu thuê phải là một ngày hợp lệ.',
            'ngaydutra.date' => 'Ngày dự kiến trả phải là một ngày hợp lệ.',
            'ngaydutra.after_or_equal' => 'Ngày dự kiến trả phải sau ngày bắt đầu thuê.',
            'tamung.numeric' => 'Tiền tạm ứng chỉ gồm chữ số.',
            'tamung.min' => 'Tiền tạm ứng phải lớn hơn hoặc bằng 0.',
        ];
    }
}
