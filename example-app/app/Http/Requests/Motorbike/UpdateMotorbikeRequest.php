<?php

namespace App\Http\Requests\Motorbike;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateMotorbikeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        $motorbikeId = $this->route('motorbike');
        return [
            'tenxe' => 'required|max:255',
            'hangxe' => 'required|max:255',
            'bienso' => ['required',
                        'regex:/^[0-9]{2}[A-Z]{1}[0-9]{1}-[0-9]{4,5}$/',
                         Rule::unique('motorbikes')->ignore($motorbikeId)],
            'giathue' => 'required|numeric|min:0',
            'status' => 'required|in:Còn trống,Đang bận',
            // 'file_upload' => 'required'
        ];


    }


    public function messages()
{
    return [
        'tenxe.required' => 'Vui lòng nhập tên xe.',
        'tenxe.max' => 'Tên xe không được vượt quá :max ký tự.',
        'hangxe.required' => 'Vui lòng nhập hãng xe.',
        'hangxe.max' => 'Hãng xe không được vượt quá :max ký tự.',
        'bienso.required' => 'Vui lòng nhập biển số.',
        'bienso.unique' => 'Biển số này đã tồn tại trong hệ thống.',
        'bienso.regex' => 'Biển số phải có định dạng tương tự 12A1-1234(5).',
        'giathue.required' => 'Vui lòng nhập giá.',
        'giathue.numeric' => 'giá chỉ gồm chữ số',
        'giathue.min' => 'giá phải lớn hơn hoặc bằng 0',
        'status.required' => 'Vui lòng nhập trạng thái.',
        'status.in' => 'Trạng thái chỉ có thể là "Còn trống" hoặc "Đang bận"'
    ];
}
}
