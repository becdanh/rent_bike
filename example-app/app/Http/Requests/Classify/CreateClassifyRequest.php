<?php

namespace App\Http\Requests\Classify;

use Illuminate\Foundation\Http\FormRequest;

class CreateClassifyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'tenphanloai' => 'required|unique:classifybikes',
        ];
    }

    public function messages()
    {
        return [

            'tenphanloai.required' => 'Vui lòng không để trống.',
            'tenphanloai.unique' => 'Phân loại xe này đã tồn tại.',
            
        ];
    }
}
