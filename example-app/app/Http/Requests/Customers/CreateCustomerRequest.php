<?php

namespace App\Http\Requests\Customers;

use Illuminate\Foundation\Http\FormRequest;

class CreateCustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'hoten' => 'required|max:255|regex:/^[\p{L} ]+$/u',
            'sdt' => 'required|unique:customers|regex:/^[0-9]{10,11}$/',
            'cccd' => 'required|unique:customers|regex:/^[0-9]{9}(?:[0-9]{3})?$/',
            'banglai' => 'required|unique:customers|regex:/^[0-9]{12}$/',
            'cccd1' => 'required',
            'cccd2' => 'required'
        ];
    }

    public function messages()
{
    return [
        'hoten.required' => 'Vui lòng nhập họ tên.',
        'hoten.max' => 'Họ tên không được vượt quá :max ký tự.',
        'hoten.regex'=>'Tên chỉ được chứa các ký tự chữ cái',
        'sdt.required' => 'Vui lòng nhập số điện thoại.',
        'sdt.unique' => 'Số điện thoại đã tồn tại trong hệ thống.',
        'sdt.regex' => 'Số điện thoại phải có độ dài từ 10 đến 11 chữ số.',
        'cccd.required' => 'Vui lòng nhập số CCCD/CMND.',
        'cccd.unique' => 'Số CCCD đã tồn tại trong hệ thống.',
        'cccd.regex' => 'Số CCCD không hợp lệ (9 số với CMND và 12 số với CCCD).',
        'banglai.required' => 'Vui lòng nhập số bằng lái.',
        'banglai.unique' => 'Bằng lái đã tồn tại trong hệ thống.',
        'banglai.regex' => 'Số bằng lái phải có độ dài 12 chữ số.',
        'cccd1.required' => 'Vui lòng không được để trống.',
        'cccd2.required' => 'Vui lòng không được để trống.',
    ];
}
}
