<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Classifybike extends Model
{
    use HasFactory;

    protected $fillable = [
        'tenphanloai'
    ];

    public function motorbikes(){

        return $this->hasMany(Motorbike::class);
    }
}
