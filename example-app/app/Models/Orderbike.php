<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Orderbike extends Model
{
    use HasFactory;


    protected $fillable = [
        'ngaybatdauthue',
        'ngaydutra',
        'status',
        'customer_id',
        'motorbike_id',
        'tamung',
        'ghichu'
    ];

    protected $attributes = [
        'tamung' => 0,
    ];

    public function customer(){
        return $this->belongsTo(Customer::class,'customer_id');
    }

    public function orderbike_bike(){

        return $this->belongsTo(Motorbike::class,'motorbike_id');
    }

    // Định nghĩa quan hệ thuộc về hóa đơn (Bill)
    public function bill()
    {
        return $this->hasOne(Bill::class);
    }

}
