<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;

    protected $fillable = [
        'hoten',
        'sdt',
        'cccd',
        'banglai',
        'cccd1',
        'cccd2'
    
    ];

    public function orderBikes(){

        return $this->hasMany(Orderbike::class);
    }

}
