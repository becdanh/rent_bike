<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    use HasFactory;

    protected $fillable = [
        'orderbike_id',
        'ngaytra',
        'tongtien',
        'phuthu',
        'ghichu',
    ];

    protected $attributes = [
        'phuthu' => 0,
    ];

    public function orderbike()
     {
         return $this->belongsTo(Orderbike::class, 'orderbike_id');
     }
}
