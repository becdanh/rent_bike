<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Motorbike extends Model
{
    use HasFactory;

    protected $fillable = [
        'tenxe',
        'hangxe',
        'bienso',
        'giathue',
        'status',
        'classifybike_id',
        'image'
    ];


    public function classify(){
        return $this->belongsTo(Classifybike::class,'classifybike_id','id');
    }

}
