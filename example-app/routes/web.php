<?php

use App\Http\Controllers\Admin\BillController;
use App\Http\Controllers\Admin\ClassifyController;
use App\Http\Controllers\Admin\CustomerController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\MotorbikeController;
use App\Http\Controllers\Admin\OrderbikeController;
use App\Models\Classifybike;
use App\Models\Customer;

use App\Http\Controllers\Admin\Motorbikeontroller;
use App\Http\Controllers\Admin\RoleController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/dashboard',[DashboardController::class, 'index'])->name('dashboard')
->middleware('auth');





// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::resource('roles', RoleController::class);

Route::get('/motorbikes/search', [MotorbikeController::class,'search'])->name('motorbikes.search');
Route::resource('motorbikes', MotorbikeController::class)->middleware('auth');






Route::get('/classify/search', [ClassifyController::class,'search'])->name('classify.search');
Route::resource('classify',ClassifyController::class)->middleware('auth');



// Route::get('customers/create', CustomerController::class, 'create')->name('customers.create');






Auth::routes();







// // Route::resource('orderbikes',OrderbikeController::class);
// //search
// Route::get('/orderbikes/search', [OrderbikeController::class,'search'])->name('orderbikes.search');

// //orderbike
// Route::get('/orderbikes',[OrderbikeController::class, 'index'])
// ->name('orderbikes.index');



// // create orderbike
// Route::get('/orderbikes/create', [OrderbikeController::class, 'create'])
// ->name('orderbikes.create');
// Route::post('/orderbikes/store', [OrderbikeController::class, 'store'])
// ->name('orderbikes.store');
// //show
// Route::get('orderbikes/{id}', [OrderbikeController::class,'show'])
// ->name('orderbikes.show');

// //edit orderbike
// Route::get('/orderbikes/edit/{id}', [OrderbikeController::class, 'edit'])
// ->name('orderbikes.edit');
// Route::put('/orderbikes/update/{id}', [OrderbikeController::class, 'update'])
// ->name('orderbikes.update');

// //delete
// Route::delete('/orderbikes/delete/{id}', [OrderbikeController::class, 'destroy'])
// ->name('orderbikes.destroy');


Route::middleware('auth')->group(function () {
    // Search Orderbikes
    Route::get('/orderbikes/search', [OrderbikeController::class, 'search'])->name('orderbikes.search');

    // Show Orderbikes
    Route::get('/orderbikes', [OrderbikeController::class, 'index'])->name('orderbikes.index');

    // Create Orderbike
    Route::get('/orderbikes/create', [OrderbikeController::class, 'create'])->name('orderbikes.create');
    Route::post('/orderbikes/store', [OrderbikeController::class, 'store'])->name('orderbikes.store');

    // Show Orderbike details
    Route::get('/orderbikes/{id}', [OrderbikeController::class, 'show'])->name('orderbikes.show');

    // Edit Orderbike
    Route::get('/orderbikes/edit/{id}', [OrderbikeController::class, 'edit'])->name('orderbikes.edit');
    Route::put('/orderbikes/update/{id}', [OrderbikeController::class, 'update'])->name('orderbikes.update');

    // Delete Orderbike
    Route::delete('/orderbikes/delete/{id}', [OrderbikeController::class, 'destroy'])->name('orderbikes.destroy');
});





// //customers
// Route::get('/customers',[CustomerController::class, 'index'])
// ->name('customers.index');

// // create customers
// Route::get('/customers/create', [CustomerController::class, 'create'])
// ->name('customers.create');
// Route::post('/customers/store', [CustomerController::class, 'store'])
// ->name('customers.store');

// //edit customers
// Route::get('/customers/edit/{id}', [CustomerController::class, 'edit'])
// ->name('customers.edit');
// Route::put('/customers/update/{id}', [CustomerController::class, 'update'])
// ->name('customers.update');

// //delete
// Route::delete('/customers/delete/{id}', [CustomerController::class, 'destroy'])
// ->name('customers.destroy');

// //search
// Route::get('/customers/search', [CustomerController::class,'search'])->name('customers.search');


// Route::post('/customers/store-ajax', [CustomerController::class,'storeAjax'])->name('customers.storeAjax');

Route::middleware('auth')->group(function () {
    Route::get('/customers', [CustomerController::class, 'index'])->name('customers.index');
    Route::get('/customers/create', [CustomerController::class, 'create'])->name('customers.create');
    Route::post('/customers/store', [CustomerController::class, 'store'])->name('customers.store');
    Route::get('/customers/edit/{id}', [CustomerController::class, 'edit'])->name('customers.edit');
    Route::put('/customers/update/{id}', [CustomerController::class, 'update'])->name('customers.update');
    Route::delete('/customers/delete/{id}', [CustomerController::class, 'destroy'])->name('customers.destroy');
    Route::get('/customers/search', [CustomerController::class, 'search'])->name('customers.search');
    Route::post('/customers/store-ajax', [CustomerController::class, 'storeAjax'])->name('customers.storeAjax');
});









Route::get('/bills/search', [BillController::class,'search'])->name('bills.search');

Route::resource('bills', BillController::class)->middleware('auth');
Route::post('/bills/store', [BillController::class, 'store'])
->name('bills.store')->middleware('auth');
