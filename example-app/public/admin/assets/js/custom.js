// public/js/custom.js

$(document).ready(function() {
  $('#addCustomerForm').on('submit', function(event) {
        $.ajaxSetup({
      headers: {
          "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
      },
    });

    event.preventDefault();

    // Lấy dữ liệu từ form
    // var formData = $('#addCustomerForm').serialize();
var formData = new FormData($('#addCustomerForm')[0]);


    // Gửi yêu cầu Ajax
    $.ajax({
      url: "/customers/store-ajax", // Thay thế bằng URL đích cho yêu cầu của bạn
      method: "POST",
      data: formData,
      contentType: false,
      processData: false,
      success: function(response) {
        // console.log(response);
        alert("Thêm khách hàng thành công!");
        $('#addCustomerModal').modal('hide'); // Ẩn modal sau khi thêm thành công
        $('#addCustomerForm')[0].reset();
        // viet function xoa data sau khi submit thanh cong : keywork: clear all data input in modal js
      },
      error: function(xhr, status, error) {

        console.log(xhr);
        // Xử lý lỗi nếu cần thiết
        var errors = xhr.responseJSON.errors;
        console.log(errors);
        if (errors) {
          // Hiển thị thông báo lỗi cho các trường dữ liệu không hợp lệ
          for (var key in errors) {
            if (errors.hasOwnProperty(key)) {
              var errorMessage = errors[key][0]; // Lấy thông báo lỗi đầu tiên trong mảng lỗi

              $(`input[name=${key}]`).next().text(errorMessage);
            }
          }}
      }
    });
  });
  console.log(123123123123);
  // Xóa thông báo lỗi khi người dùng bắt đầu nhập liệu
  $('input.form-control').on('focus', function() {
    console.log(222222);

    $(this).next('.error-message').text('');
  });
});
