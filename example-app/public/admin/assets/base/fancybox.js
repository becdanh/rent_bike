
    Fancybox.bind("[data-fancybox]", {
//   Your custom options
        Toolbar: {
            display: {
            left: ["infobar"],
            middle: [
                "zoomIn",
                "zoomOut",
                "toggle1to1",
                "rotateCCW",
                "rotateCW",
                "flipX",
                "flipY",
            ],
            right: ["download","slideshow", "thumbs","fullscreen", "close"],
            },
        },
});

