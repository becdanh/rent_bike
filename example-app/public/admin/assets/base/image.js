$(document).ready(() => {
    function readURL(input, imgId) {
        if (input.files && input.files[0]) {
            const reader = new FileReader();
            reader.onload = function(e) {
                $(imgId).attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#image-input").change(function() {
        readURL(this, '#show-image');
    });

    $("#image-input1").change(function() {
        readURL(this, '#show-image1');
    });
});
