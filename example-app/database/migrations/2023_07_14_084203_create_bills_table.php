<?php

use App\Models\Customer;
use App\Models\Motorbike;
use App\Models\Orderbike;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('bills', function (Blueprint $table) {
            $table->id();
            // $table->foreignIdFor(Customer::class)->constrained()->cascadeOnDelete();
            // $table->foreignIdFor(Motorbike::class)->constrained()->cascadeOnDelete();
            $table->foreignIdFor(Orderbike::class)->constrained()->cascadeOnDelete();
            // $table->datetime('ngaybatdauthue');
            $table->datetime('ngaytra');
            $table->double('tongtien');
            $table->double('phuthu')->default(0);
            $table->string('ghichu')->default('')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('bills');
    }
};