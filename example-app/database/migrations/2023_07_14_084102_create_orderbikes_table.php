<?php

use App\Models\Customer;
use App\Models\Motorbike;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('orderbikes', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Customer::class)->constrained()->cascadeOnDelete();
            $table->foreignIdFor(Motorbike::class)->constrained()->cascadeOnDelete();
            $table->datetime('ngaybatdauthue');
            $table->dateTime('ngaydutra');
            $table->string('status');
            $table->double('tamung');
            $table->string('ghichu');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('orderbikes');
    }
};
